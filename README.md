# What is this?

This is (well, actually,  will be...) a simple graphical game of "matching cards." I am writing this because I decided that I want to learn Japanese.

> Well, I am quite a "language junkie", and if they use the Latin alphabet it is no fun... ;-) 

Yes, I know that it sounds like a non-sequitur, but bear with me. The problem with Japanese is that it has three writing systems: katakana, hiragama e kanji.  The first two are syllabe-based and every "letter" represents a syllabe. The third one is derived from chinese and every sign is a word. Katakan and hiragama are a bit simpler since they have only (!) 50 signs, while Kanji has thousands (it seems that ~1500 suffices to survive).  Actually, 50 signs is approximately twice the size of an alphabet, so it is not so tragic after all...

In order to learn all these symbols in a less boring way, I decided to write a small game (having fun also while writing the game...)

> This reminds me when my brother was studying Lating in high school and he decided to write a small program in Basic on the ZX Spectrum (yes, we are that old...) to learn the name declinations. After debugging the code he already knew everything...  Will be the same for me?

# How do you play the game?

The program has a number of virtual "cards" with a "recto" and a "verso". In the recto there is, say, the hiragama symbol and in the verso its pronunciation. Imagine the two halves can be separated. A number N of cards is drawn, the halves separated (obtaining 2N halves)  and placed on the table.

The user select a recto and a verso and if the two match, they are removed, otherwise they are de-selected. The user can ask for help and a random correct pair is shown. The program measure the time required to complete a table and a keep a record of the scores. The dimension of the table can be changed by the user.

The game is configurable in the sense that a deck of cards is represented by a directory that contains image files, two images per card, one for the recto and one for the verso.  Virtual decks can be built as unions of normal decks.

> How do we match the two files for the same card? The easiest way could be a convention on the name (e.g. the recto ends in '-recto' and the verso in '-verso'), it could use two different dirs (e.g. `recto/` and `verso/`) or the matching could be done using a configuration file. Maybe the first solution is the best one, it has the advantage of showing recto and verso one next the other when shown on a file navigator.

# Technical details

## State machine

* The program starts with no card selected
* If the user click on a recto
  * If no verso is currently selected, the recto is selected
  * If a verso is selected it is checked if the two cards match
    * If they do, the cards are removed
    * If they do not, the cards are de-selected (and shuffled again?)
* If the user clicks on a verso, the program has a behaviour similar to the recto case
* If the user clicks on help, a correct pair is shown and the cards shuffled
* If the user clicks on quit, etc. the corresponding action is done

## Code structure

### Cards

Every card has
* A "side" value that assumes the values `(Recto, Verso)`
* An "image" value with the image to be painted on the screen
* A "rotation" value 
* An "area" value representing the actual active area on the screen. The card 
* A "z" value to be used to disambiguate between different cards on the same screen area.

Or maybe we can differentiate between the *screen tile* that has a card associated and the card itself. The card has 
* A "name"
* Recto or Verso
* An image to be shown, with dimensions in pixels

The screen tile has associated
* A card
* A rotation
* Possibly a "z" value
* It can reply to `Is_Inside` to say if a point is within the area.

The program could run as this
* Load the required set of cards to make a deck of full cards
* Select a random subset of cards, enough to cover the table
* Split every card in recto and verso to make a deck of sides
* Shuffle the deck
* Create a set of screen tiles, in z order
* Assign a side card to every screen tile 
* Draw the tile deck
* Start a timer

Now everything is ready, we enter the main loop where a process waits for events and send them to an engine

